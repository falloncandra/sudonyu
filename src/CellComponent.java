import java.awt.Font;

import javax.swing.JTextField;


public class CellComponent extends JTextField{
	private int row;
    private int col;
    private int value;
	public CellComponent(int row, int col){
		this.row = row;
		this.col = col;
		setColumns(2);
		setFont(new Font("SansSerif",Font.PLAIN,20));
		setHorizontalAlignment(JTextField.CENTER);
	}
	
    public int getRow(){
        return row;
    }

    public int getCol(){
        return col;
    }
    
    public String getContent(){
    	return this.getText();
    }
    
    public void setContent(int val)
    {
    	this.value=val;
    }
    
    
    public int getGroup(){
    	for(int i=1; i<=16; i+=4){
    		if(row+1>=i && row+1<=i+3){
        		if(col+1>=1 && col+1<=4){
        			return i;
        		}
        		else if(col+1>=5 && col+1<=8){
        			return i+1;
        		}
        		else if(col+1>=9 && col+1<=12){
        			return i+2;
        		}
        		else if(col+1>=13 && col+1<=16){
        			return i+3;
        		}
        	}	
    	}
    	return 0;
    }
    
}
