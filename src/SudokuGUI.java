import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class SudokuGUI extends JFrame {
	private JButton solveButton;
	private JButton otherSolButton;
	private BoardComponent board1;
	
	public SudokuGUI(){
		board1 = new BoardComponent();
		
		setTitle("Sudoku 4x4");
		setSize(920,660);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
		
		JPanel mainPanel = new JPanel(); 
        mainPanel.setLayout(new BorderLayout());
        
        JPanel titlePanel = new JPanel();
        JLabel titleLabel = new JLabel("Sudoku 4x4");
        titleLabel.setFont(new Font("Monospaced", Font.BOLD, 40));
        titlePanel.add(titleLabel);
        mainPanel.add(titlePanel, BorderLayout.NORTH);
        
        JPanel panel = new JPanel();
        mainPanel.add(panel, BorderLayout.WEST); 
        
        String inst = "<html>How to Play<br>"
        		+ "1. Put number 1-16 on the grid<br>"
        		+ "2. Click \"Solve\" to get the solution<br>"
        		+ "3. Click \"Other Solutions\" to get another solution</html>";
        JLabel insLabel = new JLabel(inst);
        
        JPanel panelInstruction = new JPanel();        
        panelInstruction.add(insLabel, BorderLayout.WEST);
        insLabel.setBorder(BorderFactory.createEmptyBorder(100,20,30,30));

        mainPanel.add(panelInstruction, BorderLayout.CENTER);
        
        JPanel panelBoard1 = new JPanel();
        panelBoard1.add(board1);
        
        panel.add(panelBoard1);
        
        JPanel panelButton = new JPanel();
        panelButton.setLayout(new GridLayout(1,3));
        panelButton.setBorder(BorderFactory.createEmptyBorder(10, 50, 15, 50));
        
        solveButton = new JButton("Solve");
        solveButton.addActionListener(new SolveListener());
        panelButton.add(solveButton);
        
        otherSolButton = new JButton("Other Solutions");
        otherSolButton.addActionListener(new OtherSolListener());
        otherSolButton.setEnabled(false);
        panelButton.add(otherSolButton);
        
        JButton newBoardButton = new JButton("New Board");
        newBoardButton.addActionListener(new NewBoardListener());
        panelButton.add(newBoardButton);
        
        mainPanel.add(panelButton, BorderLayout.SOUTH);
        
        getContentPane().add(mainPanel);
	}

	private class SolveListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
				solveButton.setEnabled(false);
				boolean isSat = board1.solve();
				if(isSat){
					otherSolButton.setEnabled(true);					
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class OtherSolListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try {
				boolean isSat = board1.findOtherSolution();
				if(!isSat){
					otherSolButton.setEnabled(false);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class NewBoardListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			board1.reset(); //reset jtextfield
			solveButton.setEnabled(true);
		}
	}
	
	public static void main (String args[]){
    	SudokuGUI app = new SudokuGUI ();
        app.setVisible (true);
	}
}
