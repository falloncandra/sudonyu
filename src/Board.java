import javax.swing.JOptionPane;

public class Board
{

    /** ukuran default papan */
    public static final int DEFAULT_GRID_SIZE = 16;

    /** panjang minimum kapal */
    public static final int MIN_SHIP_LENGTH = 2;

    // flag/tanda/status dari masing-masing sel pada board
    // TODO: BONUS bisa diganti menggunakan enum
    // nanti jika sudah belajar enum, ini bisa diganti menggunakan enum (BONUS)
    public static final char EMPTY = '.';
    public static final char SHIP = 'S';
    public static final char HIT = 'H';
    public static final char MISS = 'M';

    public static final boolean HORIZONTAL = true;
    public static final boolean VERTICAL = false;

    /** jumlah baris dalam grid */
    private int height;

    /** jumlah kolom dalam grid */
    private int width;

    /** grid / papan bermain */
    private String[][] grid;
    
    /**
     * Membuat sebuah board baru yang kosong dengan ukuran default.
     */
    public Board ()
    {
        this (DEFAULT_GRID_SIZE, DEFAULT_GRID_SIZE);
    }

    /**
     * Membuat sebuah board baru yang kosong.
     */
    public Board (int height, int width)
    {
        this.height = height;
        this.width = width;

        grid = new String[height][width];

        for (int rr = 0; rr < height; rr++) {
            for (int cc = 0; cc < width; cc++) {
                grid[rr][cc] = "-";
            }
        }
    }


    /**
     * Mengembalikan lebar dari grid.
     * 
     * @return jumlah kolom
     */
    public int getWidth ()
    {
        return width;
    }


    /**
     * Mengembalikan tinggi dari grid.
     * 
     * @return jumlah baris
     */
    public int getHeight ()
    {
        return height;
    }

/*
    public boolean placeShip (Ship ship)
    {
        return placeShip (ship.getLength (), ship.getRow (), ship.getCol (),
                ship.isHorizontal ());
    }
*/

    
    /**
     * Mengambil isi dari Board
     */
    public String getContents ()
    {
        StringBuilder result = new StringBuilder ();
        for (int rr = 0; rr < height; rr++) {
            for (int cc = 0; cc < width; cc++) {
            	result.append (grid[rr][cc]);
            }
            result.append ("\n");
        }

        return result.toString ();
    }





    /**
     * Memeriksa apakah koordinat yang diberikan valid.
     * 
     * @param row
     *            koordinat baris (0 ... height - 1)
     * @param col
     *            koordinat kolom (0 ... width - 1)
     * @return true jika koordinat valid, false jika koordinat invalid
     */
    public boolean isValidCoordinate (int row, int col)
    {
        return (row >= 0) && (col >= 0) && (row < height) && (col < width);
    }


    public static String rowColToCoord (int row, int col)
    {
        return String.format ("%c%d", (char) ('A' + col), row + 1);
    }


    /**
     * Mendapatkan posisi baris dari koordinat yang diberikan. Contoh:
     * coordToRow("B3") mengembalikan 2, coordToRow("G10") mengembalikan 9.
     * 
     * @param coord
     *            koordinat seperti "A1", "J10", "a1", "j10"
     * @return posisi baris (0 ... height - 1)
     */
    public static int coordToRow (String coord)
    {
        return Integer.parseInt (coord.substring (1).trim ()) - 1;
    }


    /**
     * Mendapatkan posisi kolom dari koordinat yang diberikan. Contoh:
     * coordToCol("B3") mengembalikan 1, coordToRow("G10") mengembalikan 6.
     * 
     * @param coord
     *            koordinat seperti "A1", "J10", "a1", "j10"
     * @return posisi kolom (0 ... width - 1)
     */
    public static int coordToCol (String coord)
    {
        return Character.toUpperCase (coord.charAt (0)) - 'A';
    }


    public String getContent (int row, int col)
    {
        return grid[row][col];
    }
    
    //TODO invalid value exception
    public class DoubleHitException extends RuntimeException {
    	public DoubleHitException() {
    		JOptionPane.showMessageDialog(null, "This ship has been hit\n Hit another place", "Double Hit", JOptionPane.ERROR_MESSAGE);
    	}
    	
    	public DoubleHitException(String message)
    	{
    		super (message);
    	}
    }
    
    public boolean reset(){
    	for (int rr = 0; rr < height; rr++) {
            for (int cc = 0; cc < width; cc++) {
            	grid[rr][cc]="-";
            }
        }
    	return true;
    }

}
