import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.SwingConstants;

public class GridsShowQuestion
{
  private static final int GAP = 10;
  private static final int ROWS = 3;
  private static final int COLS = ROWS;
  private static final int FONT_SIZE = 50;
  private JPanel mainPanel = new JPanel();

  public GridsShowQuestion()
  {
    mainPanel.setBackground(Color.red);
    mainPanel.setLayout(new GridLayout(ROWS, COLS, GAP, 0));
    for (int i = 0; i < ROWS; i++)
    {
      for (int j = 0; j < COLS; j++)
      {
        JLabel lbl = new JLabel("X");
        lbl.setFont(new Font(Font.DIALOG, Font.BOLD, FONT_SIZE));
        int eb = 10;
        lbl.setBorder(BorderFactory.createEmptyBorder(eb, 3*eb, eb, 3*eb));
        lbl.setHorizontalAlignment(SwingConstants.CENTER);
        lbl.setOpaque(true);
        mainPanel.add(lbl);
      }
    }
  }

  public JComponent getComponent()
  {
    return mainPanel;
  }

  private static void createAndShowUI()
  {
    JFrame frame = new JFrame("GridsShowQuestion");
    frame.getContentPane().add(new GridsShowQuestion().getComponent());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }

  public static void main(String[] args)
  {
    java.awt.EventQueue.invokeLater(new Runnable()
    {
      public void run()
      {
        createAndShowUI();
      }
    });
  }
}