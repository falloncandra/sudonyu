import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTextPane;


public class BoardComponent extends JPanel {
    private CellComponent[][] cells;
    private int height;
    private int width;
    public Color color1, color2;
    public int group;

    public BoardComponent()
    {
        //inisialisasi komponent BoardComponent
    	color1 = Color.WHITE;
    	color2 = new Color(237,251,255);
    	height=16;
    	width=16;
    	setLayout(new GridLayout(height,width));
    	cells = new CellComponent[height][width];
    	for(int i=0; i<height; i++)
    	{
    		for(int j=0; j<width; j++)
    		{
    			cells[i][j] = new CellComponent(i, j);
    			group = cells[i][j].getGroup();
    			if(group==1||group==3||group==6||group==8||group==9||group==11||group==14||group==16){
        			cells[i][j].setBackground(color1);    				
    			}
    			else{
        			cells[i][j].setBackground(color2);
    			}

    			add(cells[i][j]);
        		setBorder(BorderFactory.createEmptyBorder(0,10,10,0));
    		}
    	}
    }
    

    public void reset(){
    	color1 = Color.WHITE;
    	color2 = new Color(237,251,255);
    	for(int i=0; i<height; i++)
    	{
    		for(int j=0; j<width; j++)
    		{
    			cells[i][j].setText("");
    			cells[i][j].setEnabled(true);
    			group = cells[i][j].getGroup();
    			if(group==1||group==3||group==6||group==8||group==9||group==11||group==14||group==16){
        			cells[i][j].setBackground(color1);    				
    			}
    			else{
        			cells[i][j].setBackground(color2);
    			}
    		}
    	}
    }
    
    public boolean solve() throws IOException{
    	boolean isSat = false;
    	String val ="";
    	StringBuilder sb = new StringBuilder();
    	ArrayList<String> newClauses = new ArrayList<String>();
    	for(int i=0; i<height; i++){
    		for(int j=0; j<width; j++){
    			val = cells[i][j].getContent();
    			if(!val.equals("")){
    				val=(Integer.parseInt(val)-1)+"";
    				cells[i][j].setBackground(new Color(248,255,176));
    				sb = new StringBuilder();
    				sb.append(i); //row
    				sb.append(" ");
    				sb.append(j); //col
    				sb.append(" ");
    				sb.append(val); //value
    				newClauses.add(sb.toString());
    			}
    		}
    	}
    	SudokuMiniSAT input = new SudokuMiniSAT();
    	System.out.println("generating clauses...");
    	StringBuilder clauses = new StringBuilder();
    	clauses.append(input.generateInput());
    	clauses.append(input.addClauses(newClauses));
    	input.printInput(clauses.toString());
    	System.out.println("solving Sudoku...");
    	input.runMiniSAT();
    	
    	ArrayList<int[]> result = input.getResult();
    	
    	if(!result.isEmpty())
    	{
    		isSat = true;
    		for(int i=0;i<result.size()-1;i++)
    		{
    			int[] cell = result.get(i);
    			int value = cell[2]+1;

    			cells[cell[0]][cell[1]].setText(""+value);
    			cells[cell[0]][cell[1]].setContent(value);
    			cells[cell[0]][cell[1]].setEnabled(false);
    			cells[cell[0]][cell[1]].setDisabledTextColor(Color.gray);
    		}
    	}
    	return isSat;

    }

    public boolean findOtherSolution() throws IOException{
    	boolean isSat = false;
    	System.out.println("finding another solution...");
    	//read output minisat u/ solusi sebelumnya
    	String resultFile = "minisat.out";
    	BufferedReader in = new BufferedReader(new FileReader(resultFile));
    	in.readLine();
    	String resultClause = in.readLine();
    	String negasi ="";
    	String[] result = resultClause.split(" ");
    	for(int i=0; i<result.length; i++){
    		int temp = Integer.parseInt(result[i])*(-1);
    		if(i==0){
        		negasi += temp;    			
    		}
    		else{
        		negasi += " " + temp;    			
    		}
    	}
    	in.close();
    	
    	//read input minisat u/ problem sebelumnya
    	String inputFile = "minisat.in";
    	BufferedReader inputRead = new BufferedReader(new FileReader(inputFile));
    	StringBuilder read = new StringBuilder();
    	String line = "";
    	String line1 = inputRead.readLine();
    	String oldCount = "";
    	if(line1!=null){
    		oldCount = line1.substring(11);
    	}
    	while((line = inputRead.readLine()) != null){
    		read.append(line);
    		read.append("\n");
    	}
    	inputRead.close();
    	
    	//write input minisat baru
    	BufferedWriter input = new BufferedWriter(new FileWriter(inputFile));
    	line = "";
    	input.write("p cnf 4096 "+ (Integer.parseInt(oldCount)+1) + "\n");
    	input.write(read.toString());
    	input.write(negasi+"\n");
    	input.close();
    	
    	SudokuMiniSAT other = new SudokuMiniSAT();
    	other.runMiniSAT();
    	
    	ArrayList<int[]> resultOther = other.getResult();
    	if(!resultOther.isEmpty())
    	{
    		isSat = true;
    		for(int i=0;i<resultOther.size()-1;i++)
    		{
    			int[] cell = resultOther.get(i);
    			int value = cell[2]+1;

    			cells[cell[0]][cell[1]].setEnabled(true);
    			cells[cell[0]][cell[1]].setText(""+value);
    			cells[cell[0]][cell[1]].setContent(value);
    			cells[cell[0]][cell[1]].setEnabled(false);
    		}
    	}	
		JOptionPane.showMessageDialog(null, "Other Solution Found", "information",JOptionPane.INFORMATION_MESSAGE);
		return isSat;
    }
}
