import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;

import javax.swing.JOptionPane;



public class SudokuMiniSAT {
	static File file;
	static FileWriter fw;
	static BufferedWriter bw;
	static int[][] varTable;
	static int countClause;
	
	public SudokuMiniSAT (){
		this.countClause=0;
	}
		
	/* rule 1: each field contains at least one digit + no field contains two digits
	 * rule 2: each digit at least once in each row + and not more than once
	 * rule 3: each digit at least once in each column + and not more than once
	 * rule 4: each digit at least once in each group (little blocks) + and not more than once
	 * */
	
	public static String generateInput() throws IOException{
		StringBuilder initialClauses = new StringBuilder();	
		countClause=0;
		initialClauses.append(generateClauseRule1());
		initialClauses.append(generateClauseRule2());
		initialClauses.append(generateClauseRule3());
		initialClauses.append(generateClauseRule4());
		return initialClauses.toString();
	}
	
	public static String addClauses(ArrayList<String> sudokuCells) throws IOException{
		String newClauses = "";		
		for(String cell : sudokuCells){
			String[] temp = cell.split(" ");
			int minisat = sudokuToMinisatVar(Integer.parseInt(temp[0]),Integer.parseInt(temp[1]),Integer.parseInt(temp[2]));
			newClauses = newClauses + minisat + " 0\n";
			countClause++;
		} 
		return newClauses;
	}
	
	public static String generateClauseRule1() throws IOException{
		String rule1="";
		
		/* Each field contains at least one digit
		 * for row 0-15, for column 0-15
		 * V[i,j,0], V[i,j,1], ... V[i,j,15] 
		 */
		int temp=0;
		for(int row=0; row<=15; row++){
			for(int col=0; col<=15; col++){
				temp = sudokuToMinisatVar(row,col,0);
				rule1 = rule1 + temp;
				for(int val=1; val<=15; val++){
					temp = sudokuToMinisatVar(row,col,val);
					rule1 = rule1 + " " + temp;
				}
				rule1 = rule1 + " 0\n";
				countClause++;
			}
		}
		
		// No field contains two digit
		temp=0;
		int temp2=0;
		for(int row=0; row<=15; row++){
			for(int col=0; col<=15; col++){
				for(int valA=0; valA<=14; valA++){
					for(int valB=valA+1; valB<=15; valB++){
						temp=sudokuToMinisatVar(row, col, valA);
						temp2=sudokuToMinisatVar(row, col, valB);
						rule1 = rule1 + '-' + temp + " -" + temp2 + " 0\n";	
						countClause++;
					}
				}
			}
		}
		return rule1;
	}
	
	public static String generateClauseRule2() throws IOException{
		String rule2 = "";
		
		/* Each digit must occurs at least once in each row.
		 * for column 0-15, for value 0-15 
		 * V[0,j,k], V[1,j,k], ... V[15,j,k]
		 * */
		int temp=0;
		for(int col=0; col<=15; col++){
			for(int val=0; val<=15; val++){
				temp = sudokuToMinisatVar(0,col,val);
				rule2 = rule2 + temp;
				for(int row=1; row<=15; row++){
					temp = sudokuToMinisatVar(row,col,val);
					rule2 = rule2 + " " + temp;
				}
				rule2 = rule2 + " 0\n";
				countClause++;
			}
		}
		
		/* and not more than once.
		 * for column 0-15, for value 0-15 for 0<=rowA<rowB<=15
		 * {~V[rowA,column,value], ~V[rowB,column,value]}
		 * */
		temp=0;
		int temp2=0;
		for(int col=0; col<=15; col++){
			for(int val=0; val<=15; val++){
				for(int rowA=0; rowA<=14; rowA++){
					for(int rowB=rowA+1; rowB<=15; rowB++){
						temp=sudokuToMinisatVar(rowA, col, val);
						temp2=sudokuToMinisatVar(rowB, col, val);
						rule2 = rule2 + '-' + temp + " -" + temp2 + " 0\n";	
						countClause++;
					}
				}
			}
		}
		return rule2;
	}
	
	public static String generateClauseRule3() throws IOException{
		String rule3="";
		
		/* Each digit must occurs at least once in each column.
		 * for row 0-15, for value 0-15
		 * V[i,0,k], V[i,1,k], ... V[i,15,k] 
		 */
		int temp=0;
		for(int row=0; row<=15; row++){
			for(int val=0; val<=15; val++){
				temp = sudokuToMinisatVar(row,0,val);
				rule3 = rule3 + temp;
				for(int col=1; col<=15; col++){
					temp = sudokuToMinisatVar(row,col,val);
					rule3 = rule3 + " " + temp;
				}
				rule3 = rule3 + " 0\n";
				countClause++;
			}
		}
		
		//and not more than once.
		temp=0;
		int temp2=0;
		for(int row=0; row<=15; row++){
			for(int val=0; val<=15; val++){
				for(int colA=0; colA<=14; colA++){
					for(int colB=colA+1; colB<=15; colB++){
						temp=sudokuToMinisatVar(row, colA, val);
						temp2=sudokuToMinisatVar(row, colB, val);
						rule3 = rule3 + '-' + temp + " -" + temp2 + " 0\n";	
						countClause++;
					}
				}
			}
		}
		return rule3;
	}
	
	public static String generateClauseRule4(){
		String rule4 = "";
		
		//Each digit must occurs at least once in each group/little block.
		int temp = 0;
		//loop group
		for(int rowGroup=0; rowGroup<16; rowGroup+=4){
			for(int colGroup=0; colGroup<16; colGroup+=4){
				//loop value
				for(int val=0; val<=15; val++){
					//loop cell
					for(int rowCell=0; rowCell<4; rowCell++){
						for(int colCell=0; colCell<4; colCell++){
							int row=rowGroup+rowCell;
							int col=colGroup+colCell;
							temp = sudokuToMinisatVar(row,col,val);
							if(rowCell==0 && colCell==0){
								rule4 = rule4 + temp;
							}
							else{
								rule4 = rule4 + " " + temp;								
							}
						}
					}
					rule4 = rule4 + " 0\n";
					countClause++;
				}
			}
		}

		//and not more than once.
		//group -> value -> cell
		temp=0;
		int temp2=0;
		//loop group
		for(int rowGroup=0; rowGroup<16; rowGroup+=4){
			for(int colGroup=0; colGroup<16; colGroup+=4){
				//loop value
				for(int val=0; val<=15; val++){
					//loop cell
					for(int rowCell=0; rowCell<4; rowCell++){
						for(int colCell=0; colCell<4; colCell++){
							for(int rowLain=rowCell; rowLain<4; rowLain++){
								for(int colLain=colCell; colLain<4; colLain++){
									if(rowLain!=rowCell || colLain!=colCell){
										temp=sudokuToMinisatVar(rowCell, colCell, val);
										temp2=sudokuToMinisatVar(rowLain, colLain, val);
										rule4 = rule4 + '-' + temp + " -" + temp2 + " 0\n";
										countClause++;
									}
								}
							}
						}
					}
				}
			}
		}
		return rule4;
	}
	
	public static int sudokuToMinisatVar(int row, int column, int value){
		return row*256+column*16+value+1;
	}
	
	public int[] minisatVartoSudoku(int minisatVar){
		int row = (minisatVar-1)/256;
		int column = ((minisatVar-1)%256)/16;
		int value = ((minisatVar-1)%256)%16;
		int[] sudoku = new int[3];
		sudoku[0]=row;
		sudoku[1]=column;
		sudoku[2]=value;
		return sudoku;
	}
	
	public static void printInput(String clauses) throws IOException{
		file = new File("minisat.in");

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		fw = new FileWriter(file.getAbsoluteFile());
		bw = new BufferedWriter(fw);
		
		bw.write("p cnf 4096 "+countClause+"\n");
		bw.write(clauses);
		bw.close();
	}
	
	public void runMiniSAT()
	{
		String dir = System.getProperty("user.dir");
		String command = "minisat "+dir+"\\minisat.in "+ dir +"\\minisat.out";
		
		try{
			Process process = Runtime.getRuntime().exec(command);
			process.waitFor();
		}
		catch (IOException e){
			e.printStackTrace();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		finally
		{
			System.out.println("Done.");
		}
	}
	
	public ArrayList<int[]> getResult()
	{
		String dir = System.getProperty("user.dir");
		String resultFile = dir+"\\minisat.out";
		
		ArrayList<int[]> result = new ArrayList<int[]>();
	
		try {
			BufferedReader in = new BufferedReader(new FileReader(resultFile));
			
			String status = in.readLine();
			
			if(status.equalsIgnoreCase("UNSAT"))
			{
				JOptionPane.showMessageDialog(null, "No Solution Available", "Warning",JOptionPane.WARNING_MESSAGE);
			}
			else
			{
				String res = in.readLine();
				String[] vars = res.split(" ");
				
				for(int i=0;i<vars.length;i++)
				{
					if(vars[i].charAt(0)!='-')
					{
						int satVar = Integer.parseInt(vars[i]);
						int[] sudokuVal = minisatVartoSudoku(satVar);
						result.add(sudokuVal);
					}
				}
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		finally{
			return result;
		}	
	}
}
